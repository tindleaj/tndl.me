+++
title = "Home"
+++

# [Austin Tindle](/)

_Software Engineer_

\-\-\-

I build things with computers.

Primary tools include JavaScript and TypeScript. Experience with Rust, Python, and Go.

[Resume](https://www.linkedin.com/in/austintindle/). [Blog](./blog).

<p>&nbsp;</p>

[tindleaj@gmail.com](mailto:tindleaj@gmail.com)

[github.com/tindleaj](https://github.com/tindleaj)

[twitter.com/tindleaj](https://twitter.com/tindleaj)

[medium.com/@tindleaj](https://medium.com/@tindleaj)
